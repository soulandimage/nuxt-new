class ArrayStack:
    def __init__(self):
        self._data = []

    def __len__(self):
        return len(self._data)

    def is_empty(self):
        return len(self._data) == 0

    def push(self, e):
        self._data.append(e)

    def top(self):
        if self.is_empty():
            raise IndexError('Stack is empty')
        return self._data[-1]

    def pop(self):
        if self.is_empty():
            raise IndexError("Stack is empty")
        return self._data.pop()



def is_matched_html(raw):
    # raw or raw html string
    order = ArrayStack()
    ff = []
    opening_sign = '<' or "<"
    closing_sign = '>' or ">"
    end_indicator = '/' or "/"


    S = ArrayStack()
    j = raw.find(opening_sign)
    while j != -1:
        k = raw.find(closing_sign, j + 1)
        if k == -1:
            return False
        tag = raw[j+1:k]

        # extract and post the tag name
        # if opening a tag then append to stack
        if not tag.startswith(end_indicator):
            S.push(tag)
            order = raw[0] + raw[j]
        else:
            if S.is_empty():
                return False
            if tag[1:] != S.pop():
                print('not found')


        j = raw.find(opening_sign, k + 1)
    return S.is_empty()



#target = "<html><body><h1>hello</h1><p>this is sample of <span>first</span>question</p></body></html>"
target = "<mother><child1><wrong></child1></mother>"
is_matched_html(target)